package pt.duartemf.naivecoin.handler

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.slf4j.LoggerFactory
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Component
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler
import pt.duartemf.naivecoin.domain.Block
import pt.duartemf.naivecoin.domain.Transaction
import pt.duartemf.naivecoin.event.BroadcastingEvent
import pt.duartemf.naivecoin.service.BlockchainService
import pt.duartemf.naivecoin.service.TransactionService
import pt.duartemf.naivecoin.ws.MessageType
import pt.duartemf.naivecoin.ws.WebSocketMessage
import java.util.concurrent.CopyOnWriteArrayList

/**
 * WebSocket handler for managing blockchain-related WebSocket connections and messages.
 *
 * @property objectMapper The ObjectMapper used for JSON serialization and deserialization.
 * @property blockchainService The service responsible for blockchain operations.
 */
@Component
class BlockchainWebSocketHandler(
    private val objectMapper: ObjectMapper,
    private val blockchainService: BlockchainService,
    private val eventPublisher: ApplicationEventPublisher,
    private val transactionService: TransactionService,
) : TextWebSocketHandler() {
    private val sessions = CopyOnWriteArrayList<WebSocketSession>()

    /**
     * Retrieves the list of currently connected WebSocket sessions.
     *
     * @return A list of remote addresses of the connected sessions.
     */
    fun getSessions(): List<String> = sessions.map { it.remoteAddress.toString() }

    /**
     * Called after a new WebSocket connection is established.
     *
     * @param session The newly established WebSocket session.
     */
    override fun afterConnectionEstablished(session: WebSocketSession) {
        sessions.add(session)
        sendMessage(session, WebSocketMessage(MessageType.QUERY_LATEST, null))
    }

    /**
     * Called after a WebSocket connection is closed.
     *
     * @param session The closed WebSocket session.
     * @param status The status of the closed connection.
     */
    override fun afterConnectionClosed(
        session: WebSocketSession,
        status: CloseStatus,
    ) {
        sessions.remove(session)
    }

    /**
     * Handles incoming WebSocket text messages.
     *
     * @param session The WebSocket session that sent the message.
     * @param message The received text message.
     */
    override fun handleTextMessage(
        session: WebSocketSession,
        message: TextMessage,
    ) {
        try {
            val blockchainMessage: WebSocketMessage = objectMapper.readValue(message.payload)
            handleMessage(session, blockchainMessage)
        } catch (e: Exception) {
            LOGGER.error("Error parsing message: ${message.payload}", e)
            session.close(CloseStatus.BAD_DATA)
        }
    }

    /**
     * Handles a parsed WebSocket message.
     *
     * @param session The WebSocket session that sent the message.
     * @param message The parsed message.
     */
    private fun handleMessage(
        session: WebSocketSession,
        message: WebSocketMessage,
    ) {
        when (message.type) {
            MessageType.QUERY_LATEST -> sendMessage(session, responseLatestMessage())
            MessageType.QUERY_ALL -> sendMessage(session, responseChainMessage())
            MessageType.RESPONSE_BLOCKCHAIN -> {
                val receivedBlocks: List<Block> =
                    try {
                        objectMapper.readValue(message.data ?: "[]")
                    } catch (e: Exception) {
                        LOGGER.error("Received invalid blocks: ${message.data}", e)
                        session.close(CloseStatus.BAD_DATA)
                        return
                    }
                handleBlockchainResponse(receivedBlocks)
            }
            MessageType.QUERY_TRANSACTION_POOL -> sendMessage(session, responseUnconfirmedTransactionPoolMessage())
            MessageType.RESPONSE_TRANSACTION_POOL -> {
                val receivedTransactions: List<Transaction> =
                    try {
                        objectMapper.readValue(message.data ?: "[]")
                    } catch (e: Exception) {
                        LOGGER.error("Received invalid transactions: ${message.data}", e)
                        session.close(CloseStatus.BAD_DATA)
                        return
                    }
                receivedTransactions.forEach {
                    try {
                        transactionService.addToUnconfirmedTransactionPool(it)
                    } catch (e: Exception) {
                        LOGGER.warn("Failed operation to add transaction to the unconfirmed transaction pool when receiving a broadcast", e)
                    }
                }
            }
        }
    }

    /**
     * Handles the response containing the blockchain.
     *
     * @param receivedBlocks The list of received blocks.
     */
    private fun handleBlockchainResponse(receivedBlocks: List<Block>) {
        if (receivedBlocks.isEmpty()) {
            LOGGER.warn("Received blockchain of size 0")
            return
        }
        val latestBlockReceived = receivedBlocks.last()
        val latestBlockHeld = blockchainService.getLatestBlock()
        if (latestBlockReceived.index > latestBlockHeld.index) {
            LOGGER.info(
                "Blockchain version on this node is possibly behind. It has: ${latestBlockHeld.index}. " +
                    "Peer has: ${latestBlockReceived.index}",
            )
            if (latestBlockHeld.hash == latestBlockReceived.previousHash) {
                if (blockchainService.addNewBlockToChain(latestBlockReceived)) {
                    eventPublisher.publishEvent(BroadcastingEvent(this, responseLatestMessage()))
                }
            } else if (receivedBlocks.size == 1) {
                // this situation is important to check separately because we can receive responses with either the whole chain or just the latest block (so if we get JUST the latest block, and it is of a higher index than our current latest, that means we need to query the peers in the network for the whole chain)
                LOGGER.warn("Requires querying the chain from our peer")
                eventPublisher.publishEvent(BroadcastingEvent(this, WebSocketMessage(MessageType.QUERY_ALL, null)))
            } else {
                // Not only is the received blockchain longer than the currently held one, but it seems to be longer than by one node (otherwise it would have hit the validation that compares the previous hash of the latest received node with the hash of the latest held node)
                LOGGER.info("Received longer blockchain than the version on this node, replacing.")
                blockchainService.replaceBlockChain(receivedBlocks.toMutableList())
            }
        } else {
            LOGGER.info("The blockchain received isn't longer than the version on this node. Doing nothing.")
        }
    }

    /**
     * Sends a message to a WebSocket session.
     *
     * @param session The WebSocket session to send the message to.
     * @param message The message to send.
     */
    private fun sendMessage(
        session: WebSocketSession,
        message: WebSocketMessage,
    ) {
        val messageText = objectMapper.writeValueAsString(message)
        session.sendMessage(TextMessage(messageText))
    }

    /**
     * Broadcasts a message to all connected WebSocket sessions.
     *
     * @param message The message to broadcast.
     */
    fun broadcast(message: WebSocketMessage) {
        sessions.forEach { session ->
            if (session.isOpen) {
                sendMessage(session, message)
            }
        }
    }

    /**
     * Creates a response message containing the entire blockchain.
     *
     * @return The response message.
     */
    private fun responseChainMessage() =
        WebSocketMessage(
            MessageType.RESPONSE_BLOCKCHAIN,
            objectMapper.writeValueAsString(blockchainService.getBlockchain()),
        )

    /**
     * Creates a response message containing the latest block.
     *
     * @return The response message.
     */
    private fun responseLatestMessage() =
        WebSocketMessage(
            MessageType.RESPONSE_BLOCKCHAIN,
            objectMapper.writeValueAsString(listOf(blockchainService.getLatestBlock())),
        )

    /**
     * Creates a response message containing the unconfirmed transaction pool contents.
     *
     * @return The response message.
     */
    private fun responseUnconfirmedTransactionPoolMessage() =
        WebSocketMessage(
            MessageType.RESPONSE_BLOCKCHAIN,
            objectMapper.writeValueAsString(listOf(transactionService.getUnconfirmedTransactionPool())),
        )

    companion object {
        private val LOGGER = LoggerFactory.getLogger(BlockchainWebSocketHandler::class.java)
    }
}
