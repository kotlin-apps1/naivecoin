package pt.duartemf.naivecoin.event

import org.springframework.context.ApplicationEvent
import pt.duartemf.naivecoin.ws.WebSocketMessage

class BroadcastingEvent(source: Any, val message: WebSocketMessage) : ApplicationEvent(source)
