package pt.duartemf.naivecoin.event

import org.slf4j.LoggerFactory
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component
import pt.duartemf.naivecoin.handler.BlockchainWebSocketHandler

@Component
class BlockchainEventListener(private val blockchainWebSocketHandler: BlockchainWebSocketHandler) {
    @EventListener
    fun handleBroadcastingEvent(event: BroadcastingEvent) {
        LOGGER.info("Broadcasting Event triggered")
        blockchainWebSocketHandler.broadcast(event.message)
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(BlockchainEventListener::class.java)
    }
}
