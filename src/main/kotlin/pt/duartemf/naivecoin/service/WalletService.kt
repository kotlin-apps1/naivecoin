package pt.duartemf.naivecoin.service

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import pt.duartemf.naivecoin.domain.UnspentTransactionOutput
import pt.duartemf.naivecoin.utils.derivePublicKeyHexStringFromPrivateKeyHexString
import pt.duartemf.naivecoin.utils.generatePrivateKeyHex
import java.math.BigDecimal
import java.nio.file.Files
import java.nio.file.Paths

@Service
class WalletService {
    /**
     * Creates a new wallet for the given user ID.
     *
     * This function generates a new private key and stores it in a file located at
     * `BASE_WALLET_LOCATION` with a filename prefixed by `WALLET_PRIVATE_KEY_PREFIX` and the user ID.
     * If a wallet already exists for the given user ID, it logs a message and returns without creating a new wallet.
     *
     * @param userId The ID of the user for whom the wallet is being created.
     */
    fun createWallet(userId: String) {
        val privateKeyFilePath = getWalletPath(userId)

        // Check if the wallet already exists
        if (Files.exists(privateKeyFilePath)) {
            LOGGER.info("Wallet was already created.")
            return
        }
        // Generate a new private key
        val newPrivateKey = generatePrivateKeyHex()
        // Create parent directories if they do not exist
        Files.createDirectories(privateKeyFilePath.parent)
        // Write the new private key to the file
        Files.writeString(privateKeyFilePath, newPrivateKey)
    }

    fun getPrivateKeyFromWallet(userId: String): String = Files.readString(getWalletPath(userId))

//    fun getPublicFromWallet(walletPrivateKey: String): String =
//        convertPublicKeyToHexString(derivePublicKeyFromPrivateKeyHexString(walletPrivateKey))

    fun getPublicFromWallet(walletPrivateKey: String): String = derivePublicKeyHexStringFromPrivateKeyHexString(walletPrivateKey)

    fun getBalance(
        address: String,
        unspentTransactionOutputs: List<UnspentTransactionOutput>,
    ): BigDecimal = unspentTransactionOutputs.filter { it.address == address }.sumOf { it.amount }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(WalletService::class.java)
        private const val WALLET_PRIVATE_KEY_PREFIX = "private_key"
        private val BASE_WALLET_LOCATION = "${System.getProperty("user.dir")}/src/main/resources/wallets"

        private fun getWalletPath(userId: String) = Paths.get("${BASE_WALLET_LOCATION}/${WALLET_PRIVATE_KEY_PREFIX}_$userId")
    }
}
