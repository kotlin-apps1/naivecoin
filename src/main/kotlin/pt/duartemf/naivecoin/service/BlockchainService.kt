package pt.duartemf.naivecoin.service

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import pt.duartemf.naivecoin.domain.Block
import pt.duartemf.naivecoin.domain.Transaction
import pt.duartemf.naivecoin.domain.UnspentTransactionOutput
import pt.duartemf.naivecoin.dto.BlockRequestDTO
import pt.duartemf.naivecoin.dto.BlockTransactionRequestDTO
import pt.duartemf.naivecoin.event.BroadcastingEvent
import pt.duartemf.naivecoin.utils.calculateBlockHash
import pt.duartemf.naivecoin.utils.hashMatchesDifficulty
import pt.duartemf.naivecoin.ws.MessageType
import pt.duartemf.naivecoin.ws.WebSocketMessage
import java.math.BigDecimal
import java.time.ZoneOffset
import java.time.ZonedDateTime
import kotlin.math.pow

/**
 * Service class responsible for managing blockchain operations.
 *
 * @property BLOCK_GENERATION_INTERVAL_SECONDS The interval in seconds for block generation.
 * @property DIFFICULTY_ADJUSTMENT_INTERVAL_BLOCKS The number of blocks after which the difficulty is adjusted.
 * @property transactionService The service responsible for managing transactions.
 */
@Service
class BlockchainService(
    @Value("\${application.properties.block-generation-interval-seconds}")
    private val BLOCK_GENERATION_INTERVAL_SECONDS: Int,
    @Value("\${application.properties.difficulty-adjustment-interval-blocks}")
    private val DIFFICULTY_ADJUSTMENT_INTERVAL_BLOCKS: Int,
    private val transactionService: TransactionService,
    private val walletService: WalletService,
    private val objectMapper: ObjectMapper,
    private val eventPublisher: ApplicationEventPublisher,
) {
    private val genesisBlock =
        Block(
            0.toLong(),
            calculateBlockHash(
                0.toLong(),
                "",
                ZonedDateTime.of(2024, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toEpochSecond(),
                listOf(transactionService.getGenesisTransaction()),
                0,
                0,
            ),
            "",
            ZonedDateTime.of(2024, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC).toEpochSecond(),
            listOf(transactionService.getGenesisTransaction()),
            0,
            0,
        )
    private var blockchain: MutableList<Block> = mutableListOf(genesisBlock)

    fun getBlockchain() = blockchain

    /**
     * A function to get the block at the top of the blockchain (the latest to be added)
     * @return a Block object
     */
    fun getLatestBlock(): Block = this.blockchain.last()

    /**
     * Function that executes the insertion of a block in the chain.
     * @param newBlock a Block object representing the new block to be added to the chain
     * @return true if the block was added successfully, false otherwise
     * @throws IllegalArgumentException if the block is not valid
     */
    fun addNewBlockToChain(newBlock: Block): Boolean {
        LOGGER.info("Attempting to add new block to the chain: $newBlock")
        if (!newBlock.isBlockValid(getLatestBlock())) {
            LOGGER.error("Failed to add block")
            return false
        }
        val newUnspentTransactionOutputs =
            try {
                transactionService.processTransactions(newBlock.data, newBlock.index)
            } catch (e: Exception) {
                null
            }

        if (newUnspentTransactionOutputs == null) {
            LOGGER.error("Failed to add block (transactions are not valid)")
            return false
        }
        this.blockchain.add(newBlock)
        val oldUnspentTransactionOutputs = transactionService.getUnspentTransactionOutputs()
        transactionService.setUnspentTransactionOutputs(newUnspentTransactionOutputs)
        transactionService.updateUnconfirmedTransactionPool(oldUnspentTransactionOutputs)
        LOGGER.info("Block added successfully")
        return true
    }

    fun mineNewBlock(senderPrivateKey: String) =
        generateAndAddNewRawBlock(
            listOf(
                transactionService.getCoinbaseTransaction(
                    walletService.getPublicFromWallet(senderPrivateKey),
                    getLatestBlock().index + 1,
                ),
            ).plus(transactionService.getUnconfirmedTransactionPool()),
        )

    fun mineNewRawBlock(blockRequest: BlockRequestDTO) = generateAndAddNewRawBlock(blockRequest.transactionData)

    fun mineNewTransactionBlock(
        blockTransactionRequest: BlockTransactionRequestDTO,
        senderPrivateKey: String,
    ): Block? {
        if (!transactionService.isValidAddress(blockTransactionRequest.address)) {
            throw IllegalArgumentException("Invalid address: ${blockTransactionRequest.address}")
        }
        val coinbaseTransaction =
            transactionService.getCoinbaseTransaction(
                walletService.getPublicFromWallet(blockTransactionRequest.address),
                getLatestBlock().index + 1,
            )
        val transaction =
            transactionService.createTransaction(
                blockTransactionRequest.address,
                blockTransactionRequest.amount,
                senderPrivateKey,
            )
        return generateAndAddNewRawBlock(
            listOf(
                coinbaseTransaction,
                transaction,
            ),
        )
    }

    /**
     * Function that allows distributed nodes to replace their local blockchain with a newer version that they receive.
     * The new chain must not only be valid, but it must also have a higher cumulative difficulty than the old one.
     * Follows the 'Nakamoto consensus'.
     * If this happens the local chain is replaced by the new one, and these changes are broadcast
     * @param newBlockchain a list of Block objects representing a new blockchain version that is being communicated
     */
    fun replaceBlockChain(newBlockchain: MutableList<Block>) {
        LOGGER.info("Attempting to replace conflicts current version of the blockchain with a new one")
        val unspentTransactionOutputsForTheNewChain =
            try {
                newBlockchain.fold(listOf<UnspentTransactionOutput>()) { currentList, newBlock ->
                    transactionService.processTransactions(
                        newBlock.data,
                        newBlock.index,
                        currentList.toMutableList(),
                    )
                }
            } catch (e: Exception) {
                null
            }
        if (isChainValid(newBlockchain) &&
            unspentTransactionOutputsForTheNewChain != null &&
            getAccumulatedDifficulty(newBlockchain) > getAccumulatedDifficulty(this.blockchain)
        ) {
            LOGGER.info("New version is valid. Replacing the current one with new version")
            this.blockchain = newBlockchain
            val oldUnspentTransactionOutputs = transactionService.getUnspentTransactionOutputs()
            transactionService.setUnspentTransactionOutputs(unspentTransactionOutputsForTheNewChain)
            transactionService.updateUnconfirmedTransactionPool(oldUnspentTransactionOutputs)
            broadcastLatest()
        }
        LOGGER.info("Did not replace. The new version was either not valid or smaller than the current one")
    }

    fun getAccountBalance(address: String): BigDecimal =
        walletService.getBalance(address, transactionService.getUnspentTransactionOutputs())

    fun getBlockByHash(hash: String): Block = getBlockchain().first { it.hash == hash }

    fun getTransactionById(id: String): Transaction = getBlockchain().flatMap { it.data }.first { it.id == id }

    // - PRIVATE METHODS -

    private fun generateAndAddNewRawBlock(transactions: List<Transaction>): Block {
        val block = generateNextRawBlock(transactions)
        return if (addNewBlockToChain(block)) {
            broadcastLatest()
            block
        } else {
            throw RuntimeException("Could not add block to chain")
        }
    }

    /**
     * Function that generates a new block, given a certain raw data.
     * First it gets the latest block in the chain, the current difficulty level, creates the new index (based on the
     * last one) and timestamp, and then invokes the findBlock function to get the new Block
     * @param data the actual contents of the block (a list of transactions)
     * @return a Block object that can be inserted into the blockchain
     */
    private fun generateNextRawBlock(data: List<Transaction>): Block {
        val previousBlock = getLatestBlock()
        val difficulty = getDifficulty()
        LOGGER.debug("Difficulty is: $difficulty")
        val newIndex = previousBlock.index + 1
        val newTimestamp = ZonedDateTime.now(ZoneOffset.UTC).toEpochSecond()
        return findBlock(newIndex, previousBlock.hash, newTimestamp, data, difficulty)
    }

    /**
     * Function that evaluates if a blockchain is valid.
     * It is always valid if it is empty. Otherwise, it must start with the GENESIS_BLOCK, and all other blocks after it
     * must be valid ones
     * @param aBlockchain a list of Block objects, to be evaluated
     * @return a boolean value stating whether the chain is valid
     */
    fun isChainValid(aBlockchain: List<Block>): Boolean {
        if (aBlockchain.isEmpty()) return true
        if (aBlockchain[0].toString() != genesisBlock.toString()) return false
        for (i in 1..<aBlockchain.size) {
            if (!aBlockchain[i].isBlockValid(aBlockchain[i - 1])) return false
        }
        return true
    }

    private fun broadcastLatest(): Unit =
        eventPublisher.publishEvent(
            BroadcastingEvent(
                this,
                WebSocketMessage(
                    MessageType.RESPONSE_BLOCKCHAIN,
                    objectMapper.writeValueAsString(listOf(getLatestBlock())),
                ),
            ),
        )

    /**
     * Function that, receiving the necessary basic information to create a blockchain block, iterates through possible
     * nonce values in order to find a hash that satisfies the 'proof-of-work' condition, given the difficulty involved
     * @param index the blocks position on the chain
     * @param previousHash the hash of the previous block on the chain
     * @param timestamp a numeric value representing the creation time for this block (epoch in seconds)
     * @param data the actual contents of the block (a list of transactions)
     * @param difficulty the current difficulty level in the blockchain
     * @return a Block object created with a hash that satisfies the 'proof-of-work' condition for this chain under the specified difficulty
     */
    private fun findBlock(
        index: Long,
        previousHash: String,
        timestamp: Long,
        data: List<Transaction>,
        difficulty: Int,
    ): Block {
        var nonce: Long = 0
        while (true) {
            val hash = calculateBlockHash(index, previousHash, timestamp, data, difficulty, nonce)
            if (hashMatchesDifficulty(hash, difficulty)) {
                return Block(index, hash, previousHash, timestamp, data, difficulty, nonce)
            }
            nonce++
        }
    }

    /**
     * Function that evaluates the current state of the blockchain and returns the current difficulty level
     * Compares the index of the latest Block with the property DIFFICULTY_ADJUSTMENT_INTERVAL_BLOCKS (determines how
     * often the difficulty should be adjusted, in number of blocks). If the block is the first OR if its index is not
     * a multiple of the DIFFICULTY_ADJUSTMENT_INTERVAL_BLOCKS then the difficulty stored in the latest block is
     * returned, otherwise (if the index IS a multiple of this property) then a readjusted difficulty level is returned
     * @return a numeric value defining the difficulty level of the 'proof-of-work' challenge in the blockchain currently
     */
    private fun getDifficulty(): Int {
        val latestBlock = getLatestBlock()
        return if ((latestBlock.index % DIFFICULTY_ADJUSTMENT_INTERVAL_BLOCKS).toInt() == 0 &&
            latestBlock.index.toInt() != 0
        ) {
            getAdjustedDifficulty(latestBlock)
        } else {
            latestBlock.difficulty
        }
    }

    /**
     * Function that attempts to adjust the current blockchain difficulty.
     * It first checks on the block BEFORE the previous adjustment, in order to create  the interval of time between it
     * and the latest block (this interval will be compared with the expected amount of time to mine this number of
     * blocks, which is BLOCK_GENERATION_INTERVAL_SECONDS * DIFFICULTY_ADJUSTMENT_INTERVAL_BLOCKS).
     * If it took less than half the expected time, difficulty increases by 1, if it took more than twice the expected
     * time then it decreases by 1, and if it is in the middle (more than half but less than twice the expected) then
     * it stays the same
     * @param latestBlock a Block object representing the latest block in the chain
     * @return a numeric value for the adjusted difficulty level
     */
    private fun getAdjustedDifficulty(latestBlock: Block): Int {
        val prevAdjustmentBlock = this.blockchain[this.blockchain.lastIndex - DIFFICULTY_ADJUSTMENT_INTERVAL_BLOCKS]
        val timeExpectedInSeconds = BLOCK_GENERATION_INTERVAL_SECONDS * DIFFICULTY_ADJUSTMENT_INTERVAL_BLOCKS
        val actualTimeTakenInSeconds = latestBlock.timestamp - prevAdjustmentBlock.timestamp
        return if (actualTimeTakenInSeconds < (timeExpectedInSeconds / 2)) {
            prevAdjustmentBlock.difficulty + 1
        } else if (actualTimeTakenInSeconds > (timeExpectedInSeconds * 2)) {
            prevAdjustmentBlock.difficulty - 1
        } else {
            prevAdjustmentBlock.difficulty
        }
    }

    /**
     * Function that calculates the accumulated difficulty of a chain.
     * Given that the difficulty is represented by the number of 0s that a block's hash MUST be started with, the best
     * expression to calculate the accumulation is 2^difficulty
     * @param aBlockchain a list of Block objects representing a blockchain for which accumulated difficulty is calculated
     * @return a number representing the accumulated difficulty of the chain
     */
    private fun getAccumulatedDifficulty(aBlockchain: List<Block>) =
        aBlockchain.sumOf { BASE_POWER_FOR_CALCULATING_HASH_RATE.pow(it.difficulty) }.toLong()

    companion object {
        private val LOGGER = LoggerFactory.getLogger(BlockchainService::class.java)
        private const val BASE_POWER_FOR_CALCULATING_HASH_RATE = 2.toDouble()
    }
}
