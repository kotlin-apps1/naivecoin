package pt.duartemf.naivecoin.service

import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import org.springframework.web.socket.client.WebSocketClient
import org.springframework.web.socket.client.standard.StandardWebSocketClient
import pt.duartemf.naivecoin.handler.BlockchainWebSocketHandler

/**
 * Service class responsible for managing WebSocket connections to blockchain peers.
 *
 * @property blockchainWebSocketHandler The handler responsible for managing WebSocket events.
 */
@Service
class BlockchainPeerService(private val blockchainWebSocketHandler: BlockchainWebSocketHandler) {
    private val webSocketClient: WebSocketClient = StandardWebSocketClient()

    /**
     * Connects to a peer using the provided WebSocket address.
     * It uses a WebSocketClient to try to establish the connection, and then passes the handler as the responsible to proceed from there once it is established.
     *
     * @param peerAddress The WebSocket address of the peer to connect to.
     * @throws RuntimeException if the connection to the peer fails.
     */
    fun connectToPeer(peerAddress: String) {
        try {
            webSocketClient.execute(blockchainWebSocketHandler, peerAddress).get()
        } catch (e: Exception) {
            LOGGER.error("Failed to connect to peer: $peerAddress", e)
            throw RuntimeException("Failed to connect to peer: $peerAddress", e)
        }
    }

    /**
     * Lists the currently connected peers.
     *
     * @return A list of WebSocket session addresses representing the connected peers.
     */
    fun listPeers(): List<String> = blockchainWebSocketHandler.getSessions()

    companion object {
        private val LOGGER = LoggerFactory.getLogger(BlockchainPeerService::class.java)
    }
}
