package pt.duartemf.naivecoin.service

import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationEventPublisher
import org.springframework.stereotype.Service
import pt.duartemf.naivecoin.domain.Transaction
import pt.duartemf.naivecoin.domain.TransactionInput
import pt.duartemf.naivecoin.domain.TransactionOutput
import pt.duartemf.naivecoin.domain.UnspentTransactionOutput
import pt.duartemf.naivecoin.dto.BlockTransactionRequestDTO
import pt.duartemf.naivecoin.event.BroadcastingEvent
import pt.duartemf.naivecoin.utils.calculateTransactionId
import pt.duartemf.naivecoin.utils.convertPrivateKeyHexStringToPrivateKey
import pt.duartemf.naivecoin.utils.convertPublicKeyHexStringToPublicKey
import pt.duartemf.naivecoin.utils.derivePublicKeyHexStringFromPrivateKeyHexString
import pt.duartemf.naivecoin.utils.signDataWithKey
import pt.duartemf.naivecoin.utils.verifySignature
import pt.duartemf.naivecoin.ws.MessageType
import pt.duartemf.naivecoin.ws.WebSocketMessage
import java.math.BigDecimal
import java.util.regex.Pattern

@Service
class TransactionService(
    @Value("\${application.properties.coinbase-amount}")
    private val COINBASE_AMOUNT: BigDecimal,
    private val walletService: WalletService,
    private val unconfirmedTransactionPool: MutableList<Transaction> = mutableListOf(),
    private val objectMapper: ObjectMapper,
    private val eventPublisher: ApplicationEventPublisher,
) {
    private val genesisTransaction: Transaction =
        Transaction(
            listOf(TransactionInput("", 0, "")),
            listOf(
                TransactionOutput(
                    "042d516c5b32fa2075fc5208745f66ac56a27bc35ae61b714dd14eab6972a95b" +
                        "2136976c1ab2d9a8e549ccab01ff7363d418e5de2c7d752caa54c09be9bda86765",
                    COINBASE_AMOUNT,
                ),
            ),
        )

    private val unspentTransactionOutputs: MutableList<UnspentTransactionOutput> =
        processTransactions(listOf(genesisTransaction), 0, mutableListOf())

    fun getGenesisTransaction() = genesisTransaction

    fun getUnspentTransactionOutputs() = unspentTransactionOutputs.map { it.copy() }.toMutableList()

    fun getUnspentTransactionOutputsForAddress(address: String) = getUnspentTransactionOutputs().filter { it.address == address }

    fun setUnspentTransactionOutputs(newUnspentTransactionOutputs: List<UnspentTransactionOutput>) {
        LOGGER.info("Updating the list of unspent transaction outputs with a new one: $newUnspentTransactionOutputs")
        this.unspentTransactionOutputs.clear()
        this.unspentTransactionOutputs.addAll(newUnspentTransactionOutputs)
    }

    fun getUnconfirmedTransactionPool() = unconfirmedTransactionPool.map { it.copy() }.toMutableList()

    fun setUnconfirmedTransactionPool(newUnconfirmedTransactionPool: List<Transaction>) {
        LOGGER.info("Updating the list of unconfirmed transactions with a new one: $newUnconfirmedTransactionPool")
        this.unconfirmedTransactionPool.clear()
        this.unconfirmedTransactionPool.addAll(newUnconfirmedTransactionPool)
    }

    fun processTransactions(
        transactions: List<Transaction>,
        blockIndex: Long,
    ): List<UnspentTransactionOutput> = processTransactions(transactions, blockIndex, getUnspentTransactionOutputs())

    final fun processTransactions(
        transactions: List<Transaction>,
        blockIndex: Long,
        unspentTransactionOutputs: MutableList<UnspentTransactionOutput>,
    ): MutableList<UnspentTransactionOutput> {
        if (transactions.isEmpty() || !areTransactionsValid(transactions) || !validateBlockTransactions(transactions, blockIndex)) {
            LOGGER.warn("Invalid block transactions")
            throw RuntimeException("Invalid block transactions")
        }
        return updateUnspentTransactionOutputs(transactions, unspentTransactionOutputs).toMutableList()
    }

    /**
     * Function that signs a Transaction Input (operation that sends money to another user)
     * The input must reference an unspent transaction output (a money source), and that referenced output must have an address that matches the public key derived from the private one
     * Then the data is signed and returned
     * @param transaction a Transaction object representing the list of involved inputs and outputs
     * @param transactionInputIndex an Int field representing the transaction input index (pointing to the transaction input to sign)
     * @param privateKey a string field (in hex format) representing the private key that should be used to sign the transaction input
     * @return a string value with the signed information
     * @throws NoSuchElementException if it does not match an unspent transaction output
     */
    fun signTransactionInput(
        transaction: Transaction,
        transactionInputIndex: Long,
        privateKey: String,
    ): String {
        val transactionInput = transaction.transactionInputs[transactionInputIndex.toInt()]
        val dataToSign = transaction.id
        val referencedUnspentTransactionOutput =
            findUnspentTransactionOutputs(
                transactionInput.transactionOutputId,
                transactionInput.transactionOutputIndex,
            )
                ?: throw NoSuchElementException(
                    "Could not match an unspent transaction output with id " +
                        "${transactionInput.transactionOutputId} and index ${transactionInput.transactionOutputIndex}.",
                )
        val referencedAddress = referencedUnspentTransactionOutput.address
        if (referencedAddress != derivePublicKeyHexStringFromPrivateKeyHexString(privateKey)) {
            throw IllegalArgumentException(
                "Trying to sign an input with a private key that does not match the address referenced in the transaction input",
            )
        }
        val key = convertPrivateKeyHexStringToPrivateKey(privateKey)
        return signDataWithKey(dataToSign, key)
    }

    fun getCoinbaseTransaction(
        address: String,
        blockIndex: Long,
    ) = Transaction(
        listOf(TransactionInput("", blockIndex, "")),
        listOf(TransactionOutput(address, COINBASE_AMOUNT)),
    )

    fun isValidAddress(address: String): Boolean {
        return if (address.length != PUBLIC_KEY_SIZE) {
            LOGGER.warn("Invalid Public Key length")
            false
        } else if (!Pattern.matches(PUBLIC_KEY_REGEX, address)) {
            LOGGER.warn("Public Key must contain only hex characters")
            false
        } else if (!address.startsWith("04")) {
            LOGGER.warn("Public Key must start with the 04 prefix")
            false
        } else {
            true
        }
    }

    /**
     * Finds the unspent transaction outputs required to fulfill the desired amount.
     *
     * This function iterates over the list of owned unspent transaction outputs and accumulates their amounts
     * until the desired amount is reached or exceeded. It returns a pair containing the list of included unspent
     * transaction outputs and the leftover amount.
     *
     * @param desiredAmount the amount to be fulfilled
     * @param ownedUnspentTransactionOutputs the list of unspent transaction outputs owned by the user
     * @return a pair containing the list of included unspent transaction outputs and the leftover amount
     * @throws RuntimeException if the total amount of unspent transaction outputs is insufficient to fulfill the desired amount
     */
    fun findTransactionsForAmount(
        desiredAmount: BigDecimal,
        ownedUnspentTransactionOutputs: List<UnspentTransactionOutput>,
    ): Pair<List<UnspentTransactionOutput>, BigDecimal> {
        var currentAmount = BigDecimal.ZERO
        val includedUnspentTransactionOutputs: MutableList<UnspentTransactionOutput> = mutableListOf()
        for (unspentTransactionOutput in ownedUnspentTransactionOutputs) {
            includedUnspentTransactionOutputs.add(unspentTransactionOutput)
            currentAmount = currentAmount.plus(unspentTransactionOutput.amount)
            if (currentAmount >= desiredAmount) {
                val leftoverAmount = currentAmount.minus(desiredAmount)
                return includedUnspentTransactionOutputs to leftoverAmount
            }
        }
        throw RuntimeException("Not enough currency to execute the transaction")
    }

    /**
     * Creates a transaction to send a specified amount to a receiving address.
     *
     * This function generates a transaction that includes the necessary inputs and outputs
     * to send the specified amount to the receiving address. It signs each transaction input
     * with the sender's private key.
     *
     * @param receivingAddress the address to receive the specified amount
     * @param amountToSend the amount to be sent to the receiving address
     * @param senderPrivateKey the private key of the sender, used to sign the transaction inputs
     * @return a `Transaction` object representing the created transaction
     */
    fun createTransaction(
        receivingAddress: String,
        amountToSend: BigDecimal,
        senderPrivateKey: String,
    ): Transaction {
        val senderAddress = walletService.getPublicFromWallet(senderPrivateKey)
        val ownedUnspentTransactionOutputs = unspentTransactionOutputs.filter { it.address == senderAddress }

        val (unspentTransactionOutputsToBeIncludedInTheTransaction: List<UnspentTransactionOutput>, leftoverAmount: BigDecimal) =
            findTransactionsForAmount(
                amountToSend,
                ownedUnspentTransactionOutputs,
            )

        val unsignedTransactionInputs =
            unspentTransactionOutputsToBeIncludedInTheTransaction.map {
                TransactionInput(
                    it.transactionOutputId,
                    it.transactionOutputIndex,
                )
            }
        val transaction =
            Transaction(
                unsignedTransactionInputs,
                createTransactionOutputs(receivingAddress, senderAddress, amountToSend, leftoverAmount),
            )
        transaction.transactionInputs.forEachIndexed { index: Int, input: TransactionInput ->
            input.signature =
                signTransactionInput(transaction, index.toLong(), senderPrivateKey)
        }
        return transaction
    }

    /**
     * Creates a list of transaction outputs for a transaction.
     *
     * This function generates a list of `TransactionOutput` objects based on the provided parameters.
     * It adds a transaction output for the receiving address with the specified amount to send.
     * If there is any leftover amount, it adds another transaction output for the sender address with the leftover amount.
     *
     * @param receivingAddress the address to receive the specified amount
     * @param senderAddress the address of the sender, used for the leftover amount
     * @param amountToSend the amount to be sent to the receiving address
     * @param leftoverAmount the leftover amount to be sent back to the sender address
     * @return a list of `TransactionOutput` objects representing the transaction outputs
     */
    private fun createTransactionOutputs(
        receivingAddress: String,
        senderAddress: String,
        amountToSend: BigDecimal,
        leftoverAmount: BigDecimal,
    ): List<TransactionOutput> =
        mutableListOf<TransactionOutput>().apply {
            add(TransactionOutput(receivingAddress, amountToSend))
            if (leftoverAmount > BigDecimal.ZERO) {
                add(TransactionOutput(senderAddress, leftoverAmount))
            }
        }

    fun submitTransaction(
        transactionRequest: BlockTransactionRequestDTO,
        senderPrivateKey: String,
    ): Transaction {
        val transaction = createTransaction(transactionRequest.address, transactionRequest.amount, senderPrivateKey)
        this.addToUnconfirmedTransactionPool(transaction)
        this.broadcastUnconfirmedTransactionPool()
        return transaction
    }

    fun addToUnconfirmedTransactionPool(transaction: Transaction) {
        if (!validateTransaction(transaction) || !validateTransactionWithPool(transaction)) {
            throw RuntimeException("Trying to add an invalid transaction to the unconfirmed transaction pool.")
        }
        LOGGER.info("Adding transaction $transaction to the unconfirmed transaction pool.")
        this.unconfirmedTransactionPool.add(transaction)
    }

    /**
     * Updates the unconfirmed transaction pool by removing transactions that are no longer valid.
     *
     * This function filters out transactions from the unconfirmed transaction pool that have inputs
     * referencing spent transaction outputs. It logs the removed transactions and updates the pool.
     */
    fun updateUnconfirmedTransactionPool(oldUnspentTransactionOutputs: List<UnspentTransactionOutput>) {
        // Filter transactions with any input referencing a spent transaction output
        val noLongerUnconfirmedTransactions: List<Transaction> =
            getUnconfirmedTransactionPool().filter { unconfirmedTransaction ->
                unconfirmedTransaction.transactionInputs.any {
                    findUnspentTransactionOutputs(it.transactionOutputId, it.transactionOutputIndex) == null
                }
            }
        // If there are transactions to be removed, log and remove them from the pool
        if (noLongerUnconfirmedTransactions.isNotEmpty()) {
            LOGGER.info(
                "Removing the following transactions from the unconfirmed pool " +
                    "(they have either been added to a block or are invalid): $noLongerUnconfirmedTransactions",
            )
            this.setUnconfirmedTransactionPool(getUnconfirmedTransactionPool().apply { removeAll(noLongerUnconfirmedTransactions) })
        }
    }

    // - PRIVATE METHODS -

    /**
     * Validates a transaction against the unconfirmed transaction pool to ensure no duplicate inputs.
     *
     * This function checks if any of the inputs in the provided transaction are already present
     * in the unconfirmed transaction pool. If a duplicate input is found, a warning is logged
     * and the function returns false.
     *
     * The predicate is a bit weird, as it includes a logging operation...
     * Since the AND operator (&&) is used, the left side is executed first (the actual validation):
     * - If the validation returns false no log is created (the second part of the operator is not executed)
     * - If the validation returns true, then the log is printed, and because we want the value true to be passed on, the log also returns a true (true && true equals true)
     *
     * @param transaction the transaction to validate
     * @return true if the transaction has no duplicate inputs in the unconfirmed transaction pool, false otherwise
     */
    private fun validateTransactionWithPool(transaction: Transaction): Boolean {
        val poolTransactionInputs = this.unconfirmedTransactionPool.flatMap { it.transactionInputs }
        return transaction.transactionInputs.none {
            it in poolTransactionInputs &&
                LOGGER.warn(
                    "Found a duplicate input in the unconfirmed transaction pool: ${it.transactionOutputId} " +
                        "at index ${it.transactionOutputIndex}",
                ).let {
                    true
                }
        }
    }

    private fun broadcastUnconfirmedTransactionPool(): Unit =
        eventPublisher.publishEvent(
            BroadcastingEvent(
                this,
                WebSocketMessage(
                    MessageType.RESPONSE_TRANSACTION_POOL,
                    objectMapper.writeValueAsString(this.unconfirmedTransactionPool),
                ),
            ),
        )

    private fun validateBlockTransactions(
        transactions: List<Transaction>,
        blockIndex: Long,
    ): Boolean {
        val coinbase = transactions[0]
        return if (!validateCoinbaseTransaction(coinbase, blockIndex)) {
            LOGGER.warn("Invalid coinbase transaction $coinbase")
            false
        } else if (transactionsHaveTransactionOutputDuplicates(transactions)) {
            LOGGER.warn("There are duplicated transaction outputs being referenced in the transactions")
            return false
        } else {
            transactions.subList(1, transactions.size).all { validateTransaction(it) }
        }
    }

    /**
     * Returns whether a list of transactions reference transaction outputs multiple times (they can only reference each transaction output once)
     * First we convert the list of transactions to a list of transaction inputs using flatMap, then we group this list into a map
     * For the map, the key will be the transaction output ID, the values being a list of transaction inputs that reference said key
     * If a single map entry has more than 1 transaction input then there is a duplicate for said map entry/transaction output
     * @param transactions a List of Transaction objects, the list of transactions to evaluate
     * @return a boolean stating whether the list of transactions reference duplicated transaction outputs
     */
    private fun transactionsHaveTransactionOutputDuplicates(transactions: List<Transaction>): Boolean {
        val groupingByTransactionOutputId =
            transactions
                .flatMap { it.transactionInputs }
                .groupBy { it.transactionOutputId }
        return groupingByTransactionOutputId.any { it.value.size > 1 }
    }

    private fun validateCoinbaseTransaction(
        transaction: Transaction,
        blockIndex: Long,
    ): Boolean {
        return if (calculateTransactionId(
                transaction.transactionInputs,
                transaction.transactionOutputs,
            ) != transaction.id
        ) {
            LOGGER.warn(invalidTransactionIdErrorMessage(transaction.id))
            false
        } else if (transaction.transactionInputs.size != 1) {
            LOGGER.warn("Only one transaction input can be specified in a coinbase transaction")
            false
        } else if (transaction.transactionInputs[0].transactionOutputIndex != blockIndex) {
            LOGGER.warn("The transaction output index specified in the transaction input must match the block height")
            false
        } else if (transaction.transactionOutputs.size != 1) {
            LOGGER.warn("Only one transaction output can be specified in a coinbase transaction")
            false
        } else if (transaction.transactionOutputs[0].amount != COINBASE_AMOUNT) {
            LOGGER.warn("Coinbase transaction can only have a strict value for the transaction amount")
            false
        } else {
            true
        }
    }

    /**
     * Finds the unspent transaction output that matches the given transaction ID and index.
     *
     * This function searches through the provided list of unspent transaction outputs to find
     * the one that matches the specified transaction ID and index (if no list is provided,
     * it will default to search the whole list of unspent transaction outputs in the chain).
     *
     * @param transactionId the ID of the transaction to find
     * @param index the index of the transaction output to find
     * @param unspentTransactionOutputs the list of unspent transaction outputs to search in (optional, default value is the whole list of unspent transaction outputs in the chain)
     * @return the matching unspent transaction output if found, null otherwise
     */
    private fun findUnspentTransactionOutputs(
        transactionId: String,
        index: Long,
        unspentTransactionOutputs: List<UnspentTransactionOutput> = this.unspentTransactionOutputs,
    ): UnspentTransactionOutput? =
        unspentTransactionOutputs.find { it.transactionOutputId == transactionId && it.transactionOutputIndex == index }

    /**
     * Updates the list of unspent transaction outputs by processing new transactions.
     *
     * This function performs the following steps:
     * 1. Creates a list of new unspent transaction outputs from the new transactions (from the new outputs generated by said transactions).
     * 2. Creates a list of consumed transaction outputs from the new transactions (from the inputs used for these transactions).
     * 3. Filters out the consumed transaction outputs from the currently existing unspent transaction outputs.
     * 4. Adds the new unspent transaction outputs to the resulting list.
     *
     * @param newTransactions a list of new transactions to process
     * @return a list of updated unspent transaction outputs
     */
    private fun updateUnspentTransactionOutputs(
        newTransactions: List<Transaction>,
        unspentTransactionOutputs: List<UnspentTransactionOutput>,
    ): List<UnspentTransactionOutput> {
        val newUnspentTransactionOutputs =
            newTransactions.flatMap {
                it.transactionOutputs.mapIndexed { index, transactionOutput ->
                    UnspentTransactionOutput(
                        it.id,
                        index.toLong(),
                        transactionOutput.address,
                        transactionOutput.amount,
                    )
                }
            }
        val consumedTransactionOutputs =
            newTransactions.flatMap { it.transactionInputs }
                .map {
                    UnspentTransactionOutput(
                        it.transactionOutputId,
                        it.transactionOutputIndex,
                        "",
                        BigDecimal.ZERO,
                    )
                }
        val resultingUnspentTransactionOutputs =
            unspentTransactionOutputs.filterNot {
                findUnspentTransactionOutputs(
                    it.transactionOutputId,
                    it.transactionOutputIndex,
                    consumedTransactionOutputs,
                ) != null
            }.toMutableList()
        resultingUnspentTransactionOutputs.addAll(newUnspentTransactionOutputs)
        return resultingUnspentTransactionOutputs
    }

    private fun areTransactionsValid(transactions: List<Transaction>): Boolean = transactions.all { isValidTransaction(it) }

    private fun isValidTransaction(transaction: Transaction): Boolean {
        return if (!transaction.transactionOutputs.all { isValidAddress(it.address) }) {
            LOGGER.warn("Invalid Transaction Output address")
            false
        } else {
            true
        }
    }

    private fun validateTransaction(transaction: Transaction): Boolean {
        if (calculateTransactionId(transaction.transactionInputs, transaction.transactionOutputs) != transaction.id) {
            LOGGER.warn(invalidTransactionIdErrorMessage(transaction.id))
            return false
        }

        val hasAllValidTransactionInputs =
            transaction.transactionInputs.all {
                validateTransactionInput(
                    it,
                    transaction,
                )
            }
        if (!hasAllValidTransactionInputs) {
            LOGGER.warn("Some of the transaction inputs are invalid in transaction ${transaction.id}")
            return false
        }

        val totalTransactionInputAmount =
            transaction.transactionInputs.map { getTransactionInputAmount(it) }
                .reduce(BigDecimal::plus)
        val totalTransactionOutputAmount = transaction.transactionOutputs.map { it.amount }.reduce(BigDecimal::plus)
        if (totalTransactionInputAmount.compareTo(totalTransactionOutputAmount) != 0) {
            LOGGER.warn(
                "Total amount in the transaction inputs does not match total amount in" +
                    " transaction outputs for transaction ${transaction.id}",
            )
            return false
        }
        return true
    }

    private fun getTransactionInputAmount(transactionInput: TransactionInput): BigDecimal =
        findUnspentTransactionOutputs(
            transactionInput.transactionOutputId,
            transactionInput.transactionOutputIndex,
        )?.amount ?: throw NoSuchElementException("Could not find the referenced transaction output: $transactionInput")

    private fun validateTransactionInput(
        transactionInput: TransactionInput,
        transaction: Transaction,
    ): Boolean {
        val referencedTransactionOutput =
            this.unspentTransactionOutputs.find { it.transactionOutputId == transactionInput.transactionOutputId }
                ?: throw NoSuchElementException("Could not find the referenced transaction output: $transactionInput")
        val address = referencedTransactionOutput.address
        val publicKey = convertPublicKeyHexStringToPublicKey(address)
        return verifySignature(transaction.id, transactionInput.signature, publicKey)
    }

    companion object {
        private val LOGGER = LoggerFactory.getLogger(TransactionService::class.java)
        private const val PUBLIC_KEY_SIZE = 130
        private const val PUBLIC_KEY_REGEX = "^[a-fA-F0-9]+$"

        private fun invalidTransactionIdErrorMessage(transactionId: String) = "Invalid Transaction ID $transactionId"
    }
}
