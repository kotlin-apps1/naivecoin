package pt.duartemf.naivecoin.config

import org.springframework.context.annotation.Configuration
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry
import pt.duartemf.naivecoin.handler.BlockchainWebSocketHandler

/**
 * WebSocket configuration class that enables WebSocket support and registers WebSocket handlers.
 *
 * @property websocketHandler the WebSocket handler for handling WebSocket connections.
 */
@Configuration
@EnableWebSocket
class WebSocketConfig(private val websocketHandler: BlockchainWebSocketHandler) : WebSocketConfigurer {
    /**
     * Registers WebSocket handlers with the specified registry.
     *
     * @param registry the registry to register WebSocket handlers with.
     */
    override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
        registry.addHandler(websocketHandler, "/ws").setAllowedOrigins("*")
    }
}
