package pt.duartemf.naivecoin.utils

import org.bouncycastle.asn1.x9.X9ECParameters
import org.bouncycastle.crypto.ec.CustomNamedCurves
import org.bouncycastle.crypto.params.ECDomainParameters
import org.bouncycastle.jce.ECNamedCurveTable
import org.bouncycastle.jce.provider.BouncyCastleProvider
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec
import org.bouncycastle.jce.spec.ECPrivateKeySpec
import org.bouncycastle.jce.spec.ECPublicKeySpec
import org.bouncycastle.math.ec.ECPoint
import pt.duartemf.naivecoin.utils.CryptographyUtils.DOMAIN
import pt.duartemf.naivecoin.utils.CryptographyUtils.getEcdsaSignVerify
import java.math.BigInteger
import java.security.KeyFactory
import java.security.KeyPairGenerator
import java.security.PrivateKey
import java.security.PublicKey
import java.security.SecureRandom
import java.security.Security
import java.security.Signature
import java.security.interfaces.ECPrivateKey
import java.security.spec.ECGenParameterSpec
import java.util.*

/**
 * Generates a private key in hexadecimal format.
 *
 * This function uses the `CryptographyUtils.KEY_GEN` to generate a new EC private key,
 * and then converts the private key to a hexadecimal string, ensuring it is 64 characters long.
 *
 * @return a 64-character hexadecimal string representing the private key
 */
fun generatePrivateKeyHex(): String {
    val privateKey = CryptographyUtils.KEY_GEN.generateKeyPair().private as ECPrivateKey
    return privateKey.s.toString(16).padStart(64, '0')
}

fun derivePublicKeyHexStringFromPrivateKeyHexString(privateKeyString: String): String {
    // Convert string of numbers to BigInteger
    val privateKey = BigInteger(privateKeyString)
    // Validate private key is within valid range
    require(privateKey > BigInteger.ZERO && privateKey < DOMAIN.n) {
        "Private key out of valid range"
    }
    // Generate public key point
    val publicKeyPoint: ECPoint = DOMAIN.g.multiply(privateKey)
    // Convert to uncompressed format (both X and Y coordinates)
    return publicKeyPoint.getEncoded(false).toHex()
}

/**
 * Extension function to convert ByteArray to hex string
 */
private fun ByteArray.toHex(): String = joinToString(separator = "") { byte -> "%02x".format(byte) }

/**
 * Converts a private ECDSA key (in decimal string format) into a PrivateKey object for usage in cryptographic operations.
 *
 * This function performs the following steps:
 * 1. Converts the decimal string into a BigInteger.
 * 2. Retrieves the parameter specification for the EC curve.
 * 3. Initializes a KeyFactory for the specified algorithm and provider.
 * 4. Creates an ECPrivateKeySpec using the BigInteger and parameter specification.
 * 5. Generates and returns a PrivateKey object from the KeyFactory.
 *
 * @param decimalString a string in decimal format that represents the ECDSA private key
 * @return a PrivateKey object for the same private key
 */
fun convertPrivateKeyHexStringToPrivateKey(decimalString: String): PrivateKey {
    val privateKeyBigInt = BigInteger(decimalString)
    val privateKeySpec = ECPrivateKeySpec(privateKeyBigInt, CryptographyUtils.EC_CURVE_PARAM_SPEC)
    return CryptographyUtils.KEY_FACTORY.generatePrivate(privateKeySpec)
}

/**
 * Converts a public ECDSA key (in hex string format) into a PublicKey object for usage in cryptographic operations.
 *
 * This function performs the following steps:
 * 1. Converts the hex string into a BigInteger.
 * 2. Decodes the BigInteger into an EC point using the curve parameters.
 * 3. Creates an ECPublicKeySpec using the decoded EC point and curve parameters.
 * 4. Generates and returns a PublicKey object from the KeyFactory.
 *
 * @param publicHexString a string in hex format that represents the ECDSA public key
 * @return a PublicKey object for the same public key
 */
fun convertPublicKeyHexStringToPublicKey(publicHexString: String): PublicKey {
    val keySpec =
        ECPublicKeySpec(
            CryptographyUtils.EC_CURVE_PARAM_SPEC.curve.decodePoint(
                BigInteger(
                    publicHexString,
                    CryptographyUtils.HEX_RADIX,
                ).toByteArray(),
            ),
            CryptographyUtils.EC_CURVE_PARAM_SPEC,
        )
    return CryptographyUtils.KEY_FACTORY.generatePublic(keySpec)
}

/**
 * Signs data using a PrivateKey with the SHA256withECDSA algorithm.
 *
 * This function performs the following steps:
 * 1. Initializes the ECDSA signature with the provided private key.
 * 2. Updates the signature with the data to be signed.
 * 3. Signs the data and encodes the result in Base64.
 *
 * @param data a string representing the data to sign
 * @param privateKey a PrivateKey object representing the private key used to sign the data
 * @return a Base64-encoded string representing the signed data
 */
fun signDataWithKey(
    data: String,
    privateKey: PrivateKey,
): String {
    val ecdsaSign = getEcdsaSignVerify()
    ecdsaSign.initSign(privateKey)
    ecdsaSign.update(data.toByteArray(Charsets.UTF_8))
    return Base64.getEncoder().encodeToString(ecdsaSign.sign())
}

/**
 * Verifies the signature of the provided data using the given public key.
 *
 * This function performs the following steps:
 * 1. Initializes the ECDSA signature verification with the provided public key.
 * 2. Updates the verifier with the data to be verified.
 * 3. Verifies the signature by decoding it from Base64.
 *
 * @param data a string representing the data to verify
 * @param signature a Base64-encoded string representing the signature to verify
 * @param publicKey a PublicKey object representing the public key used for verification
 * @return a boolean indicating whether the signature is valid
 */
fun verifySignature(
    data: String,
    signature: String,
    publicKey: PublicKey,
): Boolean {
    val ecdsaVerify = getEcdsaSignVerify()
    ecdsaVerify.initVerify(publicKey)
    ecdsaVerify.update(data.toByteArray(Charsets.UTF_8))
    return ecdsaVerify.verify(Base64.getDecoder().decode(signature))
}

object CryptographyUtils {
    init {
        // Register the BouncyCastle provider
        Security.addProvider(BouncyCastleProvider())
    }

    private const val EC_CURVE_PARAMETER = "secp256k1"
    private const val KEY_ALGORITHM = "EC"
    private const val KEY_PROVIDER = "BC"
    private const val SIGN_ALGORITHM = "SHA256withECDSA"
    private val EC_GEN_PARAM_SPEC = ECGenParameterSpec(EC_CURVE_PARAMETER)
    private val CURVE: X9ECParameters = CustomNamedCurves.getByName("secp256k1")
    val KEY_FACTORY: KeyFactory = KeyFactory.getInstance(KEY_ALGORITHM, KEY_PROVIDER)
    val EC_CURVE_PARAM_SPEC: ECNamedCurveParameterSpec = ECNamedCurveTable.getParameterSpec(EC_CURVE_PARAMETER)
    val KEY_GEN: KeyPairGenerator =
        KeyPairGenerator.getInstance(KEY_ALGORITHM, KEY_PROVIDER).apply {
            initialize(EC_GEN_PARAM_SPEC, SecureRandom())
        }
    val DOMAIN = ECDomainParameters(CURVE.curve, CURVE.g, CURVE.n, CURVE.h)
    const val HEX_RADIX = 16

    fun getEcdsaSignVerify(): Signature = Signature.getInstance(SIGN_ALGORITHM, KEY_PROVIDER)
}
