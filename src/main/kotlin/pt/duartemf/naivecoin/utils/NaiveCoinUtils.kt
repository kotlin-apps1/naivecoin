package pt.duartemf.naivecoin.utils

import pt.duartemf.naivecoin.domain.Transaction
import pt.duartemf.naivecoin.domain.TransactionInput
import pt.duartemf.naivecoin.domain.TransactionOutput
import java.math.BigInteger
import java.security.MessageDigest

/**
 * Calculates the SHA-256 hash of the input string and encodes it as a hexadecimal string.
 *
 * This function takes an input string, computes its SHA-256 hash, and then converts the resulting byte array
 * to a hexadecimal string representation. The resulting hex string is always 64 characters long,
 * padded with leading zeros if necessary.
 *
 * @param input the string to calculate the hash for
 * @return the hexadecimal-encoded SHA-256 hash of the input string
 */
fun calculateHash(input: String): String =
    BigInteger(1, MessageDigest.getInstance("SHA-256").digest(input.toByteArray(Charsets.UTF_8))).toString(16).padStart(64, '0')

/**
 * Calculates the hash of a blockchain block based on the provided parameters using the SHA-256 algorithm.
 *
 * @param index the position of the block in the blockchain
 * @param previousHash the hash of the previous block in the blockchain
 * @param timestamp the timestamp representing the block's creation date in epoch format
 * @param data the list of transactions contained in the block
 * @param difficulty the difficulty level of the block when it was created
 * @param nonce a value that can be adjusted to generate different hashes for proof-of-work
 * @return the Base64-encoded SHA-256 hash of the block calculated using the input parameters
 */
fun calculateBlockHash(
    index: Long,
    previousHash: String,
    timestamp: Long,
    data: List<Transaction>,
    difficulty: Int,
    nonce: Long,
): String = calculateHash("$index$previousHash$timestamp$data$difficulty$nonce")

/**
 * Generates an ID for a blockchain transaction by concatenating the contents of the transaction inputs and outputs. It then calculates the SHA-256 hash of this concatenated content to create a unique identifier for the transaction.
 * @param transactionInputs a list of TransactionInput objects representing the inputs of the transaction
 * @param transactionOutputs a list of TransactionOutput objects representing the outputs of the transaction
 * @return a string representing the unique ID generated for the transaction
 */
fun calculateTransactionId(
    transactionInputs: List<TransactionInput>,
    transactionOutputs: List<TransactionOutput>,
): String {
    val transactionInputContent =
        transactionInputs.joinToString("") { "${it.transactionOutputId}${it.transactionOutputIndex}" }
    val transactionOutputContent = transactionOutputs.joinToString("") { "${it.address}${it.amount}" }
    return calculateHash("$transactionInputContent$transactionOutputContent")
}

/**
 * Converts a string to its binary format.
 *
 * This function takes a string and converts each character to its binary representation.
 * Each character is represented by an 8-bit binary string.
 *
 * @param str The string to convert.
 * @return A string containing the binary representation of the input string.
 */
private fun convertStringToBinaryString(str: String): String {
    val strBuilder = StringBuilder()
    str.toCharArray().forEach { char ->
        strBuilder.append(String.format("%8s", Integer.toBinaryString(char.code)).replace(" ", "0"))
    }
    return strBuilder.toString()
}

/**
 * Assesses if a block's hash matches the required difficulty.
 *
 * This function converts the hash to its binary representation and checks if it starts with a certain number of zeros.
 * The number of leading zeros required is determined by the difficulty parameter.
 * This is considered the 'proof-of-work' puzzle in a blockchain (needed to control how often a block can be added).
 *
 * @param hash the hash value of a certain block
 * @param difficulty a numerical parameter to control how hard it should be to register a new block in the chain
 * @return a boolean value defining whether a block's hash matches the difficulty level
 */
fun hashMatchesDifficulty(
    hash: String,
    difficulty: Int,
): Boolean {
    val binaryHash = convertStringToBinaryString(hash)
    val requiredPrefix = "0".repeat(difficulty)
    return binaryHash.startsWith(requiredPrefix)
}
