package pt.duartemf.naivecoin.dto

import jakarta.validation.Valid
import jakarta.validation.constraints.NotEmpty
import pt.duartemf.naivecoin.domain.Transaction

/**
 * Data class representing a request DTO for block creation, containing a list of transactions to be included in the block.
 *
 * @property transactionData A list of transactions to be added to the block. Must not be empty, and elements within must be valid.
 */
data class BlockRequestDTO(
    @field:NotEmpty @field:Valid val transactionData: List<Transaction>,
)
