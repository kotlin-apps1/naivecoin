package pt.duartemf.naivecoin.dto

import jakarta.validation.constraints.DecimalMin
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import java.math.BigDecimal

/**
 * Data class representing a request DTO for block transactions.
 * Contains the address and amount for a transaction.
 *
 * @property address The recipient's address for the transaction. Must not be blank.
 * @property amount The amount to be transacted. Must be not null and greater than or equal to 0.0.
 */
data class BlockTransactionRequestDTO(
    @field:NotBlank val address: String,
    @field:NotNull
    @field:DecimalMin(value = "0.0", inclusive = true)
    val amount: BigDecimal,
)
