package pt.duartemf.naivecoin.dto

import jakarta.validation.constraints.NotBlank

/**
 * Data class representing a Peer Request DTO.
 * Contains the remote address and port of a peer.
 *
 * @property remoteAddress The remote address of the peer.
 * @property remotePort The remote port of the peer.
 */
data class PeerRequestDTO(
    @field:NotBlank val remoteAddress: String,
    @field:NotBlank val remotePort: String,
)
