package pt.duartemf.naivecoin.domain

import org.slf4j.LoggerFactory
import pt.duartemf.naivecoin.utils.calculateBlockHash
import pt.duartemf.naivecoin.utils.hashMatchesDifficulty
import java.time.ZoneOffset
import java.time.ZonedDateTime

/**
 * Represents a single block in the blockchain.
 *
 * This data structure contains the following properties:
 * - `index`: a number representing the index of the block in the chain
 * - `hash`: a string representing a hash generated based on the values in all other properties of this block
 * - `previousHash`: the hash of the previous block in the chain (index - 1) to ensure immutability
 * - `timestamp`: a numeric field storing the creation timestamp for this block (in epoch format, to the second)
 * - `data`: the contents of the block (a list of transactions)
 * - `difficulty`: a numeric field storing the difficulty with which this block was created (for 'proof-of-work')
 * - `nonce`: a numeric value that can be varied to generate different hashes for the same block data (for 'proof-of-work')
 *
 * Note: This class uses a `LOGGER` instance for logging purposes.
 */
data class Block(
    val index: Long,
    val hash: String,
    val previousHash: String?,
    val timestamp: Long,
    val data: List<Transaction>,
    val difficulty: Int,
    val nonce: Long,
) {
    /**
     * Evaluates the validity of a `Block` by comparing it with a previous `Block` in the chain.
     * The block is considered valid if the following conditions are met:
     * - The `index` of the block is one greater than the `index` of the previous block.
     * - The `previousHash` of the block matches the `hash` of the previous block.
     * - The `timestamp` of the block falls within a valid range (see `isValidTimestamp` function).
     * - The calculated hash of the block matches the stored `hash`.
     *
     * @param previousBlock a Block object representing the previous block in the chain
     * @return true if the block is valid, false otherwise
     */
    fun isBlockValid(previousBlock: Block): Boolean {
        return if (previousBlock.index + 1 != this.index) {
            LOGGER.debug("Invalid Block Index")
            false
        } else if (previousBlock.hash != this.previousHash) {
            LOGGER.debug("Invalid Block PreviousHash")
            false
        } else if (!isValidTimestamp(previousBlock)) {
            LOGGER.debug("Invalid Block Timestamp")
            false
        } else if (calculateBlockHash(
                this.index,
                previousBlock.hash,
                this.timestamp,
                this.data,
                this.difficulty,
                this.nonce,
            ) != this.hash
        ) {
            LOGGER.debug(
                "Invalid Block Hash: Expected ${
                    calculateBlockHash(
                        this.index,
                        previousBlock.hash,
                        this.timestamp,
                        this.data,
                        this.difficulty,
                        this.nonce,
                    )
                } but got ${this.hash}",
            )
            false
        } else if (!hashMatchesDifficulty(this.hash, this.difficulty)) {
            LOGGER.debug(
                "Invalid Block Difficulty: Expected ${this.difficulty} but got ${this.hash}",
            )
            false
        } else {
            LOGGER.debug("Valid Block. Hash: ${this.hash}")
            true
        }
    }

    /**
     * Checks the validity of a block's timestamp.
     * It ensures that the timestamp is not less than 60 seconds in the past of its previous block
     * and not more than 60 seconds in the future of the current time.
     *
     * @param previousBlock a Block object representing the previous block in the chain
     * @return true if the block's timestamp is valid, false otherwise
     */
    private fun isValidTimestamp(previousBlock: Block) =
        (previousBlock.timestamp - 60) < this.timestamp && (this.timestamp - 60) <
            ZonedDateTime.now(
                ZoneOffset.UTC,
            ).toEpochSecond()

    companion object {
        private val LOGGER = LoggerFactory.getLogger(Block::class.java)
    }
}
