package pt.duartemf.naivecoin.domain

import jakarta.validation.Valid
import jakarta.validation.constraints.NotEmpty
import pt.duartemf.naivecoin.utils.calculateTransactionId

/**
 * Represents a transaction in the blockchain, containing information about the transaction inputs and outputs.
 *
 * @property transactionInputs a list of transaction inputs representing where the currency comes from
 * @property transactionOutputs a list of transaction outputs representing where the currency is sent
 * @property id a unique identifier for the transaction calculated based on its inputs and outputs (using SHA-256)
 */
data class Transaction(
    @field:NotEmpty @field:Valid val transactionInputs: List<TransactionInput>,
    @field:NotEmpty @field:Valid val transactionOutputs: List<TransactionOutput>,
) {
    val id: String = calculateTransactionId(transactionInputs, transactionOutputs)
}
