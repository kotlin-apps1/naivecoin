package pt.duartemf.naivecoin.domain

import jakarta.validation.constraints.DecimalMin
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import java.math.BigDecimal

/**
 * Represents a transaction output in the Naivecoin domain.
 * In this structure the address represents a public key belonging to a user (technically it should be a hash of the public key).
 *
 * @property address the address where the transaction output is being sent
 * @property amount the amount of cryptocurrency being sent in this transaction output
 */
data class TransactionOutput(
    @field:NotBlank val address: String,
    @field:NotNull @DecimalMin(value = "0.0", inclusive = true) val amount: BigDecimal,
)
