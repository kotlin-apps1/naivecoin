package pt.duartemf.naivecoin.domain

import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull

/**
 * Represents a transaction input in the Naivecoin domain.
 *
 * @property transactionOutputId the ID of the (as of yet unspent) transaction output being referenced
 * @property transactionOutputIndex the index of the (as of yet unspent) transaction output being referenced
 * @property signature the signature associated with the transaction input
 */
data class TransactionInput(
    @field:NotBlank val transactionOutputId: String,
    @field:NotNull val transactionOutputIndex: Long,
    @field:NotBlank var signature: String = "",
)
