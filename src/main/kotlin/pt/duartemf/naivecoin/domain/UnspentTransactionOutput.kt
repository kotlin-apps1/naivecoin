package pt.duartemf.naivecoin.domain

import java.math.BigDecimal

/**
 * Represents an unspent transaction output in the Naivecoin domain.
 *
 * @property transactionOutputId the unique identifier of the transaction output
 * @property transactionOutputIndex the index of the transaction output
 * @property address the address associated with the transaction output
 * @property amount the amount of cryptocurrency in the transaction output
 */
data class UnspentTransactionOutput(
    val transactionOutputId: String,
    val transactionOutputIndex: Long,
    val address: String,
    val amount: BigDecimal,
)
