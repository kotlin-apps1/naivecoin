package pt.duartemf.naivecoin.controller

import jakarta.validation.constraints.NotEmpty
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pt.duartemf.naivecoin.domain.Block
import pt.duartemf.naivecoin.dto.BlockRequestDTO
import pt.duartemf.naivecoin.dto.BlockTransactionRequestDTO
import pt.duartemf.naivecoin.service.BlockchainService

/**
 * REST controller for handling blockchain-related requests.
 *
 * @property blockchainService The service responsible for blockchain operations.
 */
@RestController
@RequestMapping("/blocks")
class BlockchainController(val blockchainService: BlockchainService) {
    /**
     * Retrieves the list of blocks in the blockchain.
     *
     * @return A list of Block objects representing the blockchain.
     */
    @GetMapping
    fun getBlocks(): List<Block> = blockchainService.getBlockchain()

    /**
     * Mines a new block and adds it to the blockchain.
     *
     * @return The newly mined Block, or null if mining fails.
     */
    @PostMapping
    fun mineBlock(
        // NOT SECURE
        @RequestParam @NotEmpty senderPrivateKey: String,
    ): Block? = blockchainService.mineNewBlock(senderPrivateKey)

    /**
     * Retrieves the list of blocks in the blockchain.
     *
     * @return A list of Block objects representing the blockchain.
     */
    @GetMapping("/{hash}")
    fun getBlockByHash(
        @PathVariable hash: String,
    ): Block = blockchainService.getBlockByHash(hash)

    /**
     * Mines a new raw block with the provided data and adds it to the blockchain.
     *
     * @param blockRequest The data for the new block.
     * @return The newly mined Block, or null if mining fails.
     */
    @PostMapping("/raw")
    fun mineRawBlock(
        @RequestBody blockRequest: BlockRequestDTO,
    ): Block? = blockchainService.mineNewRawBlock(blockRequest)

    /**
     * Mines a new transaction block with the provided transaction data and adds it to the blockchain.
     *
     * @param blockTransactionRequest The transaction data for the new block.
     * @return The newly mined Block, or null if mining fails.
     */
    @PostMapping("/transaction")
    fun mineTransactionBlock(
        @RequestBody blockTransactionRequest: BlockTransactionRequestDTO,
        // NOT SECURE
        @RequestParam @NotEmpty senderPrivateKey: String,
    ): Block? = blockchainService.mineNewTransactionBlock(blockTransactionRequest, senderPrivateKey)
}
