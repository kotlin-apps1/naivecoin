package pt.duartemf.naivecoin.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pt.duartemf.naivecoin.domain.UnspentTransactionOutput
import pt.duartemf.naivecoin.service.BlockchainService
import pt.duartemf.naivecoin.service.TransactionService
import pt.duartemf.naivecoin.service.WalletService

/**
 * REST controller for wallet-related operations.
 *
 * This controller provides endpoints for creating wallets and retrieving account balances.
 * It uses `WalletService` for wallet operations and `BlockchainService` for blockchain-related operations.
 *
 * @param walletService the service used for wallet operations
 * @param blockchainService the service used for blockchain-related operations
 */
@RestController
@RequestMapping("/wallet")
class WalletController(
    private val walletService: WalletService,
    private val blockchainService: BlockchainService,
    private val transactionService: TransactionService,
) {
    /**
     * Creates a new wallet for the specified user.
     *
     * This endpoint handles the creation of a new wallet for the user identified by the provided user ID.
     * It delegates the wallet creation process to the `WalletService`.
     *
     * @param userId the ID of the user for whom the wallet is being created
     * @return the created wallet object
     */
    @PostMapping("/{userId}")
    fun createWallet(
        @PathVariable userId: String,
    ) = walletService.createWallet(userId)

    @GetMapping("/{address}")
    fun getAddressUnspentTransactionOutputs(
        @PathVariable address: String,
    ): List<UnspentTransactionOutput> = transactionService.getUnspentTransactionOutputsForAddress(address)

    /**
     * Retrieves the balance for the specified address.
     *
     * This endpoint returns the balance of the account associated with the provided address.
     * It delegates the balance retrieval process to the `BlockchainService`.
     *
     * @param address the address of the account whose balance is being retrieved
     * @return the balance of the account
     */
    @GetMapping("/{address}/balance")
    fun getBalance(
        @PathVariable address: String,
    ) = blockchainService.getAccountBalance(address)
}
