package pt.duartemf.naivecoin.controller

import jakarta.validation.constraints.NotEmpty
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import pt.duartemf.naivecoin.domain.Transaction
import pt.duartemf.naivecoin.domain.UnspentTransactionOutput
import pt.duartemf.naivecoin.dto.BlockTransactionRequestDTO
import pt.duartemf.naivecoin.service.BlockchainService
import pt.duartemf.naivecoin.service.TransactionService

@RestController
@RequestMapping("/transactions")
class TransactionController(private val transactionService: TransactionService, private val blockchainService: BlockchainService) {
    @PostMapping
    fun submitTransaction(
        @RequestBody transactionRequest: BlockTransactionRequestDTO,
        // NOT SECURE
        @RequestParam @NotEmpty senderPrivateKey: String,
    ): Transaction =
        transactionService.submitTransaction(transactionRequest, senderPrivateKey)

    @GetMapping("/{id}")
    fun getTransactionById(
        @PathVariable id: String,
    ) = blockchainService.getTransactionById(id)

    @GetMapping("/unconfirmed")
    fun getUnconfirmedTransactions(): List<Transaction> = transactionService.getUnconfirmedTransactionPool()

    @GetMapping("/unspentOutputs")
    fun getUnspentTransactionOutputs(): List<UnspentTransactionOutput> = transactionService.getUnspentTransactionOutputs()
}
