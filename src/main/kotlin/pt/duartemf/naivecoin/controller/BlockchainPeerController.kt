package pt.duartemf.naivecoin.controller

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pt.duartemf.naivecoin.service.BlockchainPeerService

/**
 * REST controller for managing blockchain peers.
 */
@RestController
@RequestMapping("/p2p")
class BlockchainPeerController(private val blockchainPeerService: BlockchainPeerService) {
    /**
     * Retrieves the list of connected peers.
     *
     * @return a list of peer addresses.
     */
    @GetMapping("/peers")
    fun listPeers(): List<String> = blockchainPeerService.listPeers()

    /**
     * Adds a new peer to the network.
     *
     * @param address the address of the peer to connect to.
     */
    @PostMapping("/peers")
    fun addPeer(
        @RequestBody address: String,
    ): Unit = blockchainPeerService.connectToPeer(address)
}
