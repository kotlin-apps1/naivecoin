package pt.duartemf.naivecoin.ws

/**
 * Data class representing a WebSocket message.
 *
 * @property type The type of the message.
 * @property data The data of the message.
 */
data class WebSocketMessage(val type: MessageType, val data: String?)

/**
 * Enum representing the types of messages that can be handled.
 */
enum class MessageType {
    QUERY_LATEST,
    QUERY_ALL,
    RESPONSE_BLOCKCHAIN,
    QUERY_TRANSACTION_POOL,
    RESPONSE_TRANSACTION_POOL,
}
