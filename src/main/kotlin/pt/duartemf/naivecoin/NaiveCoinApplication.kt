package pt.duartemf.naivecoin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class NaiveCoinApplication

fun main(args: Array<String>) {
    runApplication<NaiveCoinApplication>(*args)
}
