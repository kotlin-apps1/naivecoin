# naivecoin

The goal of this project is to test out building a cryptocurrency (so it'll also have blockchain technology associated with it).

## Resources

- [Naivecoin Tutorial](https://lhartikk.github.io/)
- [Naivecoin TypeScript Source Code](https://github.com/lhartikk/naivecoin/tree/chapter1)
- [Base article - NaiveChain, a blockchain in 200 lines of code](https://medium.com/@lhartikk/a-blockchain-in-200-lines-of-code-963cc1cc0e54)